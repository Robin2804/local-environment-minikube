# local-environment-minikube

***

## Quick overview

Goal of this Project is, to run a single script and everything is ready to use.

Please read our [[Minikube Guide](https://the-microservice-dungeon.gitlab.io/docs/docs/infrastructure/devops-guide/minikube/)] for more information. Very important for Windows users

---

#### make sure the following programs are installed AND executable
(this will be checked)

1. minikiube
2. helm
3. kubectl
4. git

#### Make sure, thar the following files are executable
   1. firstBuildCluster.sh
   2. installInfrastructure.sh
   3. core-service-interaction.sh
   4. deleteCluster.sh


---
#### Setup minikube
1. `minikube config set memory 8192`
2. `minikube config set cpus 6`

---

#### Running `firstBuildCluster.sh` script

Run `./firstBuildCluster.sh` that install full infrastructure, and all core services

After this, you can use the cluster


Run `./firstBuildCluster.sh --help` or`./firstBuildCluster.sh -h` to see all available commands for installation

Run `./core-service-interaction.sh -h` or `./core-service-interaction.sh --help` to see help with available commands for cluster interaction for core-services


---


#### Ports for services

after `minikube tunnel` the ports will be exposed at

| service | port  |
|---------|-------|
| gamelog | 30001 |
| game    | 30002 |
| map     | 30003 |
| robot   | 30004 |
| trading | 30005 |


