#!/bin/bash


#This script contains all needed for install infrastructure on our minikube cluster
#This script must be run successfully to the end, for install core-services on our cluster
#Edit this file, when new infrastructure is added, or infrastructure is removed


# BASH COLOR TEXT CONFIG VARIABLES
# DARK_GREY='\033[1;30m'
# RED='\033[0;31m'
NOCOLOR='\033[0m'
GREEN='\033[0;32m'

#export REDPANDA_VERSION=$(curl -s https://api.github.com/repos/redpanda-data/redpanda/releases/latest | jq -r .tag_name)
REDPANDA_VERSION="v2.1.17-23.3.11"

SLEEPING_TIME=60s

# redpanda-cluster path location variables
REDPANDA_CLUSTER_CONFIG="./redpanda-cluster.yaml"


# FULL_PATH=$(realpath "$0")
# DIR_PATH=$(dirname "$FULL_PATH")
# DIR_PATH_MAIN=$(dirname "$DIR_PATH" )

# PATH_REDPANDA_CLUSTER="$(dirname "$(dirname "$(realpath "$0")")" )"/one-node-cluster.yml
#PATH_REDPANDA_CLUSTER=./one-node-cluster.yml

Text_Green(){
  echo -e "${GREEN} $1 ${NOCOLOR}"
}



# ADD NEEDED HELM REPOS
helm repo add jetstack https://charts.jetstack.io
helm repo add redpanda https://charts.redpanda.com
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add dungeon https://gitlab.com/api/v4/projects/42239222/packages/helm/stable
helm repo add bitnami https://charts.bitnami.com/bitnami

helm repo update
minikube addons enable metrics-server

# INSTALL INFRASTRUCTURE

kubectl -n default rollout status deployment

# CERT-MANAGER
helm -n cert-manager install cert-manager jetstack/cert-manager --create-namespace --set installCRDs=true

kubectl -n cert-manager rollout status deployment

# PROMETHEUS
helm -n monitoring install prometheus-stack prometheus-community/kube-prometheus-stack --create-namespace

kubectl -n monitoring rollout status deployment
kubectl -n monitoring rollout status statefulset

# REDPANDA (KAFKA)

kubectl create namespace redpanda-system
kubectl -n redpanda-system apply -k https://github.com/redpanda-data/redpanda-operator/src/go/k8s/config/crd?ref="$REDPANDA_VERSION"

helm -n redpanda-system install redpanda-operator redpanda/operator --create-namespace --set image.tag="$REDPANDA_VERSION"

kubectl -n redpanda-system rollout status deployment
kubectl -n redpanda-system rollout status statefulset

kubectl -n redpanda-system apply -f "$REDPANDA_CLUSTER_CONFIG"
sleep $SLEEPING_TIME
kubectl -n redpanda-system rollout status statefulset

# RABBITMQ
# first edit rabbitmq address to "rabbitmq.rabbitmq.svc.cluster.local:31000" for management and "rabbitmq.rabbitmq.svc.cluster.local:310001" for amqp
# in core services, and skeleton-players
# helm -n rabbitmq install rabbitmq bitnami/rabbitmq --create-namespace --set global.storageClass=standard --set clusterDomain=cluster.local --set auth.username=admin --set auth.password=admin --set securePassword=false --set service.type=LoadBalancer --set service.nodePorts.manager=31000 --set service.nodePorts.amqp=31001

# if above is edited in core-services, than end-commend the line above and delete following line
helm install rabbitmq dungeon/rabbitmq --namespace rabbitmq --create-namespace

kubectl -n rabbitmq rollout status deployment

# RABBITMQ-KAFKA-CONNECTOR
helm -n rabbitmq install kafka-rabbitmq-connector dungeon/kafka-rabbitmq-connector

kubectl -n redpanda-system rollout status statefulset
