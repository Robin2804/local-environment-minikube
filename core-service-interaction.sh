#!/bin/bash

# BASH CONFIG VARIABLES
BLUE='\033[1;34m'
RED='\033[0;31m'
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
DARK_GREY='\033[1;30m'

Text_Red () {
  echo -e "${RED} $1 ${NOCOLOR}"
}

Text_Green(){
  echo -e "${GREEN} $1 ${NOCOLOR}"
}

Text_Blue(){
  echo -e "${BLUE} $1 ${NOCOLOR}"
}

Text_Dark_Grey(){
  echo -e "${DARK_GREY} $1 ${NOCOLOR}"
}


Help(){
  echo "  =======> Generally Commands <======="
  echo " "

  echo "  -h | --help          ===> for this help"
  echo " -ur | --update-repo   ===> for update core service repo"
  echo " "
  echo " "
  echo " "
  echo "  =======> Install Core Service <======="
  echo " "

  echo " -ase | --allservice   ===> for install all core services"
  echo "  -ro | --robot        ===> for install robot core services"
  echo "  -ma | --map          ===> for install map core services"
  echo "  -tr | --trading      ===> for install trading core services"
  echo "  -ga | --game         ===> for install game core services"
  echo "  -gl | --gamelog      ===> for install gamelog core services"

  echo " "
  echo "example: install only robot and game "
  echo " ./core-service-interaction.sh -ro -ga"
  echo " OR"
  echo " ./core-service-interaction.sh -ga -ro"
  echo " "
  echo "the PARAMETERS position does not matter "
  echo " "
  echo " "


  echo "  =======> Remove Core Service <======="
  echo " "

  echo "  -dase | --del-allservice   ===> for delete all core services"
  echo "   -dro | --del-robot        ===> for delete robot core services"
  echo "   -dma | --del-map          ===> for delete map core services"
  echo "   -dtr | --del-trading      ===> for delete trading core services"
  echo "   -dga | --del-game         ===> for delete game core services"
  echo "   -dgl | --del-gamelog      ===> for delete gamelog core services"

  echo " "
  echo " "

  echo "  =======> Update Core Service <======="
  echo " "

  echo "  -uase | --up-allservice   ===> for update all core services"
  echo "   -uro | --up-robot        ===> for update robot core services"
  echo "   -uma | --up-map          ===> for update map core services"
  echo "   -utr | --up-trading      ===> for update trading core services"
  echo "   -uga | --up-game         ===> for update game core services"
  echo "   -ugl | --up-gamelog      ===> for update gamelog core services"

  echo " "
  echo " "


  echo " For more cluster interaction please refer to kubectl commands"
  echo " "

}

# ADD / UPDATE NEEDED HELM REPOS
Update_Repo(){
  helm repo remove dungeon
  helm repo add dungeon https://gitlab.com/api/v4/projects/42239222/packages/helm/stable
  helm repo update

}

Check_Rollout_Status(){
  sleep 5s
  rollout_status() {
    local resource_type=$1
    local namespace=$2
    local resource_names=$(kubectl get "$resource_type" -n "$namespace" -o jsonpath='{.items[*].metadata.name}')

    for resource_name in $resource_names; do
      Text_Dark_Grey "-----"
      echo "Checking rollout status of ${resource_type}/${resource_name} in namespace: ${namespace}"
      kubectl rollout status "${resource_type}"/"${resource_name}" -n "${namespace}"
    done
  }

  # Get the list of all namespaces
  namespaces=$(kubectl get namespaces -o jsonpath='{.items[*].metadata.name}')
  # Loop through each namespace
  for ns in $namespaces; do
    Text_Blue "__________________________________________________________________________________"
    Text_Blue "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ START ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo ""
    Text_Dark_Grey "########## Checking rollout status in namespace: $ns ##########"
    echo ""
    # Check Deployments
    rollout_status statefulset "$ns"
    # Check StatefulSets
    rollout_status deployment "$ns"
    # Check DaemonSets
    rollout_status daemonset "$ns"
    Text_Dark_Grey "-----"
    echo ""
    Text_Dark_Grey "---------> Finishing checking rollout status in namespace: $ns <---------"
    echo ""
    Text_Blue "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ END ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    Text_Blue "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
    echo ""

  done
}

Install_Chosen_Core_Services(){
for service in "$@" ; do
     case $service in
     robot)
         helm install robot dungeon/robot --namespace robot --create-namespace
       ;;

     trading)
        helm install trading dungeon/trading --namespace trading --create-namespace
       ;;

     map)
        helm install map dungeon/map --namespace map --create-namespace
       ;;

     game)
        helm install game dungeon/game --namespace game --create-namespace
       ;;

     gamelog)
        helm install gamelog dungeon/gamelog --namespace gamelog --create-namespace
       ;;
     esac
done

}

Remove_Chosen_Core_Services(){
  for service in "$@" ; do
     case $service in
     delrobot)
        helm uninstall -n robot robot
        #kubectl delete namespace robot
       ;;
     delmap)
        helm uninstall -n map map
        #kubectl delete namespace map
       ;;
     deltrading)
        helm uninstall -n trading trading
        #kubectl delete namespace trading
       ;;
     delgame)
        helm uninstall -n game game
        #kubectl delete namespace game
       ;;
     delgamelog)
        helm uninstall -n gamelog gamelog
        #kubectl delete namespace gamelog
       ;;

    esac
done
}

Update_Chosen_Core_Services(){
  for service in "$@" ; do
    case $service in
    uprobot)
         helm upgrade robot dungeon/robot --namespace robot
         ;;

    upgame)
         helm upgrade game dungeon/game --namespace game
         ;;

    uptrading)
         helm upgrade trading dungeon/trading --namespace trading
         ;;

    upmap)
         helm upgrade map dungeon/map --namespace map
         ;;

    upgamelog)
         helm upgrade gamelog dungeon/gamelog --namespace gamelog
         ;;

    esac
  done
}

if [ $# == 0 ] ; then
  echo "no core service chosen"
  Help
  else
    Update_Repo
    for var in "$@" ; do
      case $var in

    -h | --help)
      Help
      exit
      ;;

    -ur | --update-repo)
      Update_Repo
      ;;


    # Install Core Services

    -ase | --allservice)
      Install_Chosen_Core_Services "robot" "map" "trading" "game" "gamelog"
      break
      ;;

    -ro | --robot)
      Install_Chosen_Core_Services "robot"

      ;;

    -ma | --map)
      Install_Chosen_Core_Services "map"
      ;;

    -tr | --trading)
      Install_Chosen_Core_Services "trading"

      ;;

    -ga | --game)
      Install_Chosen_Core_Services "game"

      ;;

    -gl | --gamelog)
      Install_Chosen_Core_Services "gamelog"

      ;;


    # Delete Core Service

    -dro | --del-robot)
      Remove_Chosen_Core_Services "delrobot"

      ;;

     -dtr | --del-trading)
     Remove_Chosen_Core_Services "deltrading"

     ;;

    -dma | --del-map)
    Remove_Chosen_Core_Services "delmap"
    ;;

    -dga | --del-game)
    Remove_Chosen_Core_Services "delgame"
    ;;

  -dgl | --del-gamelog)
    Remove_Chosen_Core_Services "delgamelog"
    ;;

  -dase | --del-allservice)
    Remove_Chosen_Core_Services "delrobot" "delmap" "deltrading" "delgame" "delgamelog"
    break
    ;;


  # Update Core Services

  -uro | --up-robot)
    Update_Chosen_Core_Services "uprobot"
    ;;

  -uma | --up-map)
    Update_Chosen_Core_Services "upmap"
    ;;

  -utr | --up-trading)
    Update_Chosen_Core_Services "uptrading"
    ;;

  -uga | --up-game)
   Update_Chosen_Core_Services "upgame"
   ;;

  -ugl | --up-gamelog)
    Update_Chosen_Core_Services "upgamelog"
    ;;

  -uase | --up-allservice)
    Update_Chosen_Core_Services "uprobot" "upmap" "uptrading" "upgame" "upgamelog"
    break
    ;;

    *)
      echo "min. one parameter is not valid"
      exit
      ;;
    esac
  done
  Check_Rollout_Status
fi



