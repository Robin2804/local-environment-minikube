# local-environment-minikube WSL config

***

__For WSL2 \
[WSL Overview](https://learn.microsoft.com/en-us/windows/wsl/) \
[WSL in MS-Store Info](https://devblogs.microsoft.com/commandline/the-windows-subsystem-for-linux-in-the-microsoft-store-is-now-generally-available-on-windows-10-and-11/)__

---



## Preparation

1. activate feature „WSL“ in Windows features
2. load WSL from MS store
3. load Ubuntu 22.04.1 LTS from MS store




## WSL config in windows (global config)

edit / create `C:\Users\<UserName>\.wslconfig` insert following

````
# settings for all Linux distros
[wsl2]

# Limit for VM memory un GB or MB
memory=10GB

# Set VM virtual processors
processors=7

# allow nested virtualization
nestedVirtualization=true
````

run in powershell `wsl.exe --shutdown` as admin

this above give your wsl access to 10gb ram, 7 cpu cores

---
## config in Ubuntu

1. run Ubuntu installation
      1. run following for better network performance 
         1. sudo rm /etc/resolv.conf
         2. sudo bash -c 'echo "nameserver 8.8.8.8" > /etc/resolv.conf'
         3. sudo bash -c 'echo "[network]" > /etc/wsl.conf'
         4. sudo bash -c 'echo "generateResolvConf = false" >> /etc/wsl.conf'
         5. sudo chattr +i /etc/resolv.conf
      2. edit config file `sudo nano /etc/wsl.conf` (when needed)
         ```
         [network]
         generateResolvConf=false
         ```
2. run in powershell `wsl.exe --shutdown` as admin
3. config Docker Desktop (settings --> Resources --> WSL integration)
4. restart wsl2 
5. config minikube (see Readme) when needed 
6. get local-environment-minikube 
7. run ./firstBuildCluster.sh


